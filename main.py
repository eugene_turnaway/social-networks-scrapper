import selenium
from selenium import webdriver
from getpass import getpass
from login_info import numberphone, password
import datetime
import time
import pprint
j = 0
n = 0
k = int(input("Количество постов: "))
driver = webdriver.Chrome()
driver.get(url="https://m.vk.com")
posts = []
username_input = driver.find_element_by_name("email")
username_input.send_keys(numberphone)
password_input = driver.find_element_by_name("pass")
password_input.send_keys(password)
login_button = driver.find_element_by_xpath("//input[@class='button wide_button'][@type='submit']")
login_button.click()
load_page_pause = 3
SCROLL_PAUSE_TIME = 2
driver.get(url="https://m.vk.com/lovebmstu?from=groups")
time.sleep(load_page_pause)
while len(driver.find_elements_by_css_selector(".PostText, .r .cp_attached_post .pi_text, .r .wall_item ")) < k:
    last_height = driver.execute_script("return document.body.scrollHeight")
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    new_height = driver.execute_script("return document.body.scrollHeight")
    time.sleep(SCROLL_PAUSE_TIME)
    if new_height == last_height:
        break
        last_height = new_height


posts_html = driver.find_elements_by_css_selector(".PostText, .r .cp_attached_post .pi_text, .r .wall_item ")
print('ws found: '+str(len(posts_html)))
j = 0
n = 0
f = open('text.txt', 'a')
for i in range(k - 1):
    try:
        text_post = posts_html[i].find_element_by_css_selector(
            ".PostText, .r .cp_attached_post .pi_text, .r .wall_item .pi_text").text
        author_name = posts_html[i].find_element_by_css_selector(
            ".r .BookmarksItem__header .pi_author, .r .cp_attached_post .pi_author, .r .FeedItem__header .pi_author, .r .wall_item .pi_author").text
        author_link = posts_html[i].find_element_by_css_selector(
            ".r .BookmarksItem__header .pi_author, .r .cp_attached_post .pi_author, .r .FeedItem__header .pi_author, .r .wall_item .pi_author").get_attribute(
            "href")
        time_post = str(datetime.datetime.now()) + "//" + posts_html[i].find_element_by_xpath(
            "//a[@class='wi_date al_wall'][@rel='noopener']").text
        posts.append({
            'text': text_post,
            'author name': author_name,
            'author link': author_link,
            'time': time_post
        })
        f.write(text_post + ' \n')

        n += 1
    except selenium.common.exceptions.NoSuchElementException:
        j += 1
    except IndexError:
        break
        print("\n\n\nпосты кончились\n\n\n")


    print(str(round(((i + 1) * 100 / (k)), 1)) + "%")
f.close()
driver.quit()
pprint.pprint(posts)


print("\n\n\n просмотрено " + str(n+j+1) + " постов, " + str(j) + " из них без текста""\n\n\n")

